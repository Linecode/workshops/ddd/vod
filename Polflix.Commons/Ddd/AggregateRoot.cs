namespace Polflix.Commons.Ddd;

public interface AggregateRoot<out TIdentifier>
{ 
    TIdentifier Id { get; }
}