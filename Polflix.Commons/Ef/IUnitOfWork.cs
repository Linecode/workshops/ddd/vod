namespace Polflix.Commons.Ef;

public interface IUnitOfWork
{
    Task<int> SaveChangesAsync(CancellationToken cancellationToken = default);
    
    // Maybe it will be better to use union type here
    Task<bool> SaveEntitiesAsync(CancellationToken cancellationToken = default);
}
