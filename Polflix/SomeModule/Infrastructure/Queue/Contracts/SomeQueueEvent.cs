using MediatR;

namespace Polflix.SomeModule.Infrastructure.Queue.Contracts;

public record SomeQueueEvent(Guid Id) : INotification;