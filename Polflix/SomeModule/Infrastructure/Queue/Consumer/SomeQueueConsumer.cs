using System.Text.Json;
using MediatR;
using Polflix.SomeModule.Infrastructure.Queue.Contracts;

namespace Polflix.SomeModule.Infrastructure.Queue.Consumer;

public class SomeQueueEventConsumer : MassTransit.IConsumer<SomeQueueEvent>
{
    private readonly ILogger<SomeQueueEventConsumer> _logger;
    private readonly IPublisher _publisher;

    public SomeQueueEventConsumer(ILogger<SomeQueueEventConsumer> logger, IPublisher publisher)
    {
        _logger = logger;
        _publisher = publisher;
    }
    
    public async Task Consume(MassTransit.ConsumeContext<SomeQueueEvent> context)
    {
        _logger.LogInformation("SomeQueueEventConsumer: {SomeQueueEvent}", JsonSerializer.Serialize(context.Message));

        // This is not transactional
        await _publisher.Publish(context.Message);
    }
}
