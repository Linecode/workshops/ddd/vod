using Microsoft.EntityFrameworkCore;
using Polflix.Commons.Ef;
using Polflix.SomeModule.Domain;

namespace Polflix.SomeModule.Infrastructure.Persistence;

public class SomeDbContext : DbContext, IUnitOfWork
{
    public DbSet<SomeAggregate> Aggregates { get; private set; } = null!;

    public SomeDbContext(DbContextOptions<SomeDbContext> options) : base(options)
    {
        
    }
    
    public async Task<bool> SaveEntitiesAsync(CancellationToken cancellationToken = default)
    {
        await SaveChangesAsync(cancellationToken);

        return true;
    }

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        modelBuilder.ApplyConfigurationsFromAssembly(typeof(SomeDbContext).Assembly);
        
        base.OnModelCreating(modelBuilder);
    }
}
