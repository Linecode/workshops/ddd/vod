using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Polflix.SomeModule.Domain;

namespace Polflix.SomeModule.Infrastructure.Persistence.Configurations;

public class SomeAggregateEntityTypeConfiguration : IEntityTypeConfiguration<SomeAggregate>
{
    public void Configure(EntityTypeBuilder<SomeAggregate> builder)
    {
        builder.HasKey(x => x.Id);

        builder.Property(x => x.Name);

        builder.ToTable("Aggregates");
    }
}
