using MediatR;
using Polflix.SomeModule.Infrastructure.Queue.Contracts;

namespace Polflix.SomeModule.Application.Listeners;

public class SomeEventListener : INotificationHandler<SomeQueueEvent>
{
    private readonly ILogger<SomeEventListener> _logger;

    public SomeEventListener(ILogger<SomeEventListener> logger)
    {
        _logger = logger;
    }
    
    public Task Handle(SomeQueueEvent notification, CancellationToken cancellationToken)
    {
        _logger.LogInformation("SomeEventListener: Handle({Id})", notification.Id);
        
        return Task.CompletedTask;
    }
}
