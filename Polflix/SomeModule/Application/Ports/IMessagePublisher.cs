namespace Polflix.SomeModule.Application.Ports;

public interface IMessagePublisher
{
    Task Publish<T>(T obj, CancellationToken ct = default) where T : class;
}
