using MediatR;
using Polflix.Commons.Utils;
using Polflix.SomeModule.Application.Ports;
using Polflix.SomeModule.Domain;
using Polflix.SomeModule.Domain.Repositories;
using Polflix.SomeModule.Infrastructure.Queue.Contracts;

namespace Polflix.SomeModule.Application;

public record CreateSmth(string Name) : IRequest<Result<Guid>>
{
    private class CreateSmthHandler : IRequestHandler<CreateSmth, Result<Guid>>
    {
        private readonly IAggregateRepository _aggregateRepository;
        private readonly IMessagePublisher _messagePublisher;

        public CreateSmthHandler(IAggregateRepository aggregateRepository, IMessagePublisher messagePublisher)
        {
            _aggregateRepository = aggregateRepository;
            _messagePublisher = messagePublisher;
        }
        
        public async Task<Result<Guid>> Handle(CreateSmth request, CancellationToken cancellationToken)
        {
            var aggregate = new SomeAggregate(request.Name);

            try
            {
                // await _aggregateRepository.Add(aggregate);
                
                // this is not transactional
                await _messagePublisher.Publish(new SomeQueueEvent(aggregate.Id), cancellationToken);
                
                return Result<Guid>.Success(aggregate.Id);
            }
            catch (Exception ex)
            {
                return Result<Guid>.Error(new Exception("Something went wrong", ex));
            }
        }
    }
}
