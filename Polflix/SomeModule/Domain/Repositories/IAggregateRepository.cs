namespace Polflix.SomeModule.Domain.Repositories;

public interface IAggregateRepository
{
    Task Add(SomeAggregate aggregate);
}
