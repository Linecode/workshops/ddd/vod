using FluentValidation;

namespace Polflix.SomeModule.Presentation.Resources;

public record SomeResource(string Name)
{
    public class Validator : AbstractValidator<SomeResource>
    {
        public Validator()
        {
            RuleFor(x => x.Name)
                .NotEmpty()
                .WithMessage("Name is required")
                .MinimumLength(3)
                .WithMessage("Name must be at least 3 characters");
        }
    }
}
