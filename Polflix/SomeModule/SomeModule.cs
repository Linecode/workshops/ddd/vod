using Polflix.SomeModule.Application.Ports;
using Polflix.SomeModule.Domain.Repositories;
using Polflix.SomeModule.Infrastructure.Persistence;
using Polflix.SomeModule.Infrastructure.Queue;

namespace Polflix.SomeModule;

public abstract class SomeModule
{
}

public static class SomeModuleExtensions 
{
    public static IServiceCollection RegisterSomeModuleServices(this IServiceCollection services)
    {
        services.AddScoped<IAggregateRepository, AggregatesRepository>();
        services.AddScoped<IMessagePublisher, MessagePublisher>();

        return services;
    }
}
