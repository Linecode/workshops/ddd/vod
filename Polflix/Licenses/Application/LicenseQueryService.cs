using Polflix.Licenses.Infrastructure.Persistence.Licenses;

namespace Polflix.Licenses.Application;

public interface ILicenseQueryService
{
    Task<LicenseEntity?> FindById(Guid id, CancellationToken ct);
}

public class LicenseQueryService : ILicenseQueryService
{
    private readonly ILicenseQueryRepository _licenseQueryRepository;

    public LicenseQueryService(ILicenseQueryRepository licenseQueryRepository)
    {
        _licenseQueryRepository = licenseQueryRepository;
    }

    public Task<LicenseEntity?> FindById(Guid id, CancellationToken ct)
        => _licenseQueryRepository.FindById(id, ct);
}