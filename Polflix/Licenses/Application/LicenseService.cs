using Polflix.Licenses.Domain;
using Polflix.Licenses.Domain.Licensors;
using Polflix.Licenses.Infrastructure.Persistence.Licenses;
using Polflix.Licenses.Infrastructure.Persistence.Mappers;
using Polflix.Shared;

namespace Polflix.Licenses.Application;

public record CreateLicenseCommand(
    Guid LicenseId,
    DateRange TimeFrame,
    RegionIdentifier Territory,
    LicensorIdOrData Licensor, 
    string DecryptionKey
);

public record ActivateLicenseCommand(Guid LicenseId);

public record ExpireLicenseCommand(Guid LicenseId);

public record RenewLicenseCommand(
    Guid LicenseId,
    DateRange TimeFrame,
    string DecryptionKey);

public interface ILicenseService
{
    Task ProvisionLicense(CreateLicenseCommand command, CancellationToken ct);
    Task ActivateLicense(ActivateLicenseCommand command, CancellationToken ct);
    Task ExpireLicense(ExpireLicenseCommand command, CancellationToken ct);
    Task RenewLicense(RenewLicenseCommand command, CancellationToken ct);
}

public class LicenseService : ILicenseService
{
    private readonly ILicenseRepository _repository;
    private readonly License.ILicenseFactory _licenseFactory;
    private readonly ILicensorProvider _licensorProvider;

    public LicenseService(
        ILicenseRepository repository, 
        License.ILicenseFactory licenseFactory, 
        ILicensorProvider licensorProvider)
    {
        _repository = repository;
        _licenseFactory = licenseFactory;
        _licensorProvider = licensorProvider;
    }
    
    public async Task ProvisionLicense(CreateLicenseCommand command, CancellationToken ct)
    {
        var (id, dateRange, territory, licensor, decryptionKey) = command;
        var licensorEntity = await _licensorProvider.GetOrCreate(licensor, ct);

        await Handle<License.InitialLicense>(id, license =>
            License.ProvisionDecider.Provision(new License.ProvisionCommand.Create(
                id, dateRange, territory, licensorEntity, decryptionKey
            ), license), ct);
    }
    
    public Task ActivateLicense(ActivateLicenseCommand command, CancellationToken ct)
        => Handle<License.ProvisionedLicense>(command.LicenseId, license => License.ProvisionDecider.Activate(new License.ProvisionCommand.Activate(DateTime.UtcNow), license), ct);
    
    public Task ExpireLicense(ExpireLicenseCommand command, CancellationToken ct)
        => Handle<License.ActiveLicense>(command.LicenseId, license => License.ActiveDecider.Expire(new License.ActiveCommand.Expire(DateTime.UtcNow), license), ct);
    
    public Task RenewLicense(RenewLicenseCommand command, CancellationToken ct)
        => Handle<License.ExpiredLicense>(command.LicenseId, license => License.ExpireDecider.Renew(new License.ExpireCommand.Renewd(command.TimeFrame, command.DecryptionKey), license), ct);

    private Task Handle<T>(Guid id, Func<T, LicenseEvent> handle, CancellationToken ct) where T : License
        => _repository.GetAndUpdate(id, (entity) =>
        {
            var aggregate = entity?.MapToAggregate(_licenseFactory) ?? GetDefault();

            if (aggregate is not T typedLicense) throw new InvalidOperationException();

            var @event = handle(typedLicense);

            return new[] { @event };
        }, ct);
    
    private License GetDefault() => License.InitialLicense.Initial;
}