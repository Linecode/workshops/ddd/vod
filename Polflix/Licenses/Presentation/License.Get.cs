using FastEndpoints;
using Polflix.Licenses.Application;

namespace Polflix.Licenses.Presentation;

public class LicenseGet : EndpointWithoutRequest
{
    private readonly ILicenseQueryService _licenseQueryService;

    public LicenseGet(ILicenseQueryService licenseQueryService)
    {
        _licenseQueryService = licenseQueryService;
    }

    public override async Task HandleAsync(CancellationToken ct)
    {
        var id = Route<Guid>("Id");

        var entity = await _licenseQueryService.FindById(id, ct);

        await SendAsync(entity, 200, ct);
    }

    public override void Configure()
    {
        Get("/license/{Id}");
        AllowAnonymous();
    }
}