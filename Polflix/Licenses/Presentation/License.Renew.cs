using FastEndpoints;
using Polflix.Licenses.Application;
using Polflix.Licenses.Presentation.Resources;
using Polflix.Shared;

namespace Polflix.Licenses.Presentation;

public class LicenseRenew : Endpoint<RenewLicenseResource>
{
    private readonly ILicenseService _licenseService;

    public LicenseRenew(ILicenseService licenseService)
    {
        _licenseService = licenseService;
    }

    public override async Task HandleAsync(RenewLicenseResource request, CancellationToken ct)
    {
        var id = Route<Guid>("Id");

        await _licenseService.RenewLicense(
            new RenewLicenseCommand(id, DateRange.New(request.From, request.To), request.DecryptionKey), ct);
        
        await SendAsync("", 200, ct);
    }

    public override void Configure()
    {
        Post("/license/{Id}/renew");
        AllowAnonymous();
    }
}