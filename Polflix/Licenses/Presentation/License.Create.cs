using FastEndpoints;
using MediatR;
using Polflix.Licenses.Application;
using Polflix.Licenses.Domain.Licensors;
using Polflix.Licenses.Presentation.Resources;
using Polflix.Shared;

namespace Polflix.Licenses.Presentation;

public class LicenseCreate : Endpoint<CreateLicenseResource>
{
    private readonly ISender _sender;
    private readonly ILicenseService _licenseService;

    public LicenseCreate(ISender sender, ILicenseService licenseService)
    {
        _sender = sender;
        _licenseService = licenseService;
    }

    public override async Task HandleAsync(CreateLicenseResource request, CancellationToken cancellationToken)
    {
        var licenseId = Guid.NewGuid();

        var (from, to, decryptionKey, licensor, territory) = request;

        await _licenseService.ProvisionLicense(new CreateLicenseCommand(
            licenseId,
            DateRange.New(from, to),
            RegionIdentifier.Poland,
            new LicensorIdOrData(licensor.Id, licensor.Name, licensor.Address),
            decryptionKey
        ), cancellationToken);
        
        HttpContext.Response.Headers.Add("Location", $"/api/license/{licenseId}");
        await SendAsync("", 201, cancellationToken);
    }
    
    public override void Configure()
    {
        Post("/license");
        AllowAnonymous();
        Summary(new SomeEndpointSummary());
    }

    private class SomeEndpointSummary : Summary<LicenseCreate>
    {
        public SomeEndpointSummary()
        {
            Summary = "Some summary";
            Description = "Some description";
            ExampleRequest = new CreateLicenseResource(
                DateTime.UtcNow.AddDays(-1),
                DateTime.UtcNow.AddDays(1),
                "0x2cf53a5dfe75be07280d3740315247299fceeac486a9518f1268a572e1b2e4af33744b50dd7dac88d96d0db4df669bfe3a684fe731d6fb3d639f787e073c52c13b61a7a78b1b9a0848648878c2cd3c54",
                new LicensorResource(null, "test", "test@test.pl"),
                806
                );
        }
    }
}