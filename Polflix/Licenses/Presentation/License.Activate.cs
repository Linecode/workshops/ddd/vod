using FastEndpoints;
using Polflix.Licenses.Application;

namespace Polflix.Licenses.Presentation;

public class LicenseActivate : EndpointWithoutRequest
{
    private readonly ILicenseService _licenseService;

    public LicenseActivate(ILicenseService licenseService)
    {
        _licenseService = licenseService;
    }

    public override async Task HandleAsync(CancellationToken cancellationToken)
    {
        var id = Route<Guid>("Id");
        await _licenseService.ActivateLicense(new ActivateLicenseCommand(id), cancellationToken);
        
        await SendAsync("", 200, cancellationToken);
    }
    
    public override void Configure()
    {
        Post("/license/{Id}/activate");
        AllowAnonymous();
    }
}