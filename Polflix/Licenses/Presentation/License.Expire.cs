using FastEndpoints;
using Polflix.Licenses.Application;

namespace Polflix.Licenses.Presentation;

public class LicenseExpire : EndpointWithoutRequest
{
    private readonly ILicenseService _licenseService;

    public LicenseExpire(ILicenseService licenseService)
    {
        _licenseService = licenseService;
    }

    public override async Task HandleAsync(CancellationToken ct)
    {
        var id = Route<Guid>("Id");
        
        await _licenseService.ExpireLicense(new ExpireLicenseCommand(id), ct);
        
        await SendAsync("", 200, ct);
    }

    public override void Configure()
    {
        Post("/license/{Id}/expire");
        AllowAnonymous();
    }
}