namespace Polflix.Licenses.Presentation.Resources;

public record CreateLicenseResource(
    DateTime From,
    DateTime To,
    string DecryptionKey, 
    LicensorResource Licensor,
    int Territory);

public record LicensorResource(Guid? Id, string? Name, string? Address);


public record RenewLicenseResource(    
    DateTime From,
    DateTime To,
    string DecryptionKey);