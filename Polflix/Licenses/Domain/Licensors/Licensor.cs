using Polflix.Shared;

namespace Polflix.Licenses.Domain;

public class Licensor : Party
{
    public string Address { get; private set; }
    
    public Licensor(Guid id, string name, string address) : base(id, name)
    {
        Address = address;
    }
}