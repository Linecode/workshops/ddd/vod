namespace Polflix.Licenses.Domain.Licensors;

public interface ILicensorProvider
{
    Task<Licensor> GetOrCreate(LicensorIdOrData licensorIdOrData, CancellationToken ct);
}