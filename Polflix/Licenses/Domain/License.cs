using Polflix.Shared;

namespace Polflix.Licenses.Domain;

public abstract record License
{
    public enum State { Provisioned, Active, Expired }
    
    public interface ILicenseFactory
    {
        License Create(State state, DateRange dateRange, string decryptionKey);
    }

    public class Factory : ILicenseFactory
    {
        public License Create(State state, DateRange dateRange, string decryptionKey)
            => state switch
            {
                State.Active => new ActiveLicense(dateRange, decryptionKey),
                State.Expired => new ExpiredLicense(dateRange),
                State.Provisioned => new ProvisionedLicense(dateRange, decryptionKey),
                _ => throw new ArgumentOutOfRangeException(nameof(state), state, null)
            };
    }

    public abstract record LicenseCommand;
    
    public record ActiveLicense(DateRange TimeFrame, string DecryptionKey) : License;
    public record ExpiredLicense(DateRange TimeFrame) : License;
    public record ProvisionedLicense(DateRange TimeFrame, string DecryptionKey) : License;

    public record InitialLicense : License
    {
        public static InitialLicense Initial = new();
    }

    public abstract record ExpireCommand : LicenseCommand
    {
        public record Renewd(DateRange TimeFrame, string DecryptionKey) : ExpireCommand;
    }

    public static class ExpireDecider
    {
        public static LicenseEvent.LicenseRenewed Renew(ExpireCommand.Renewd command, ExpiredLicense state)
            => new (command.DecryptionKey, command.TimeFrame);
    }

    public abstract record ActiveCommand : LicenseCommand
    {
        public record Expire(DateTime Now) : ActiveCommand;
    }

    public static class ActiveDecider
    {
        public static LicenseEvent.LicenseExpired Expire(ActiveCommand.Expire command, ActiveLicense state)
        {
            // Commented out, just so we could expire any license
            // if (command.Now < state.TimeFrame.To) throw new InvalidOperationException("");

            return new LicenseEvent.LicenseExpired();
        }
    }

    public abstract record ProvisionCommand : LicenseCommand
    {
        public record Create(
            Guid LicenseId,
            DateRange TimeFrame,
            RegionIdentifier Territory,
            Licensor Licensor,
            string DecryptionKey
            ) : ProvisionCommand;

        public record Activate(DateTime Now) : ProvisionCommand;
    }

    public static class ProvisionDecider
    {
        public static LicenseEvent.LicenseAcquired Provision(ProvisionCommand.Create command, InitialLicense state)
            => new(command.DecryptionKey, command.TimeFrame, command.Territory, command.Licensor.Id);

        public static LicenseEvent.LicenseActivated Activate(ProvisionCommand.Activate command, ProvisionedLicense state)
        {
            if (!state.TimeFrame.IsWithinRange(command.Now))
                throw new InvalidOperationException("Can not activate a license that is not within its time frame.");
            
            if (string.IsNullOrWhiteSpace(state.DecryptionKey))
                throw new InvalidOperationException("Can not activate a license that is not provisioned.");

            return new();
        }
    }
}

// Domain Events
public abstract record LicenseEvent
{
    public record LicenseAcquired(string DecryptionKey, DateRange TimeFrame, RegionIdentifier Territory, Guid LicensorId) : LicenseEvent;

    public record LicenseActivated() : LicenseEvent;

    public record LicenseExpired() : LicenseEvent;

    public record LicenseRenewed(string DecryptionKey, DateRange TimeFrame) : LicenseEvent;
}

public abstract record LicenseExternalEvent
{
    private LicenseExternalEvent() { }

    public record Expired(Guid LicenseId) : LicenseExternalEvent;

    public record Activated(Guid LicenseId) : LicenseExternalEvent;
}