using System.Reflection;
using Microsoft.EntityFrameworkCore;
using Polflix.Licenses.Application;
using Polflix.Licenses.Domain;
using Polflix.Licenses.Domain.Licensors;
using Polflix.Licenses.Infrastructure.Persistence;
using Polflix.Licenses.Infrastructure.Persistence.Licenses;
using Polflix.Licenses.Infrastructure.Persistence.Licensors;

namespace Polflix.Licenses;

public abstract class LicenseModule
{
    
}

public static class LicenseModuleExtensions 
{
    public static IServiceCollection RegisterLicenseModuleServices(this IServiceCollection services, IConfiguration configuration)
    {
        services.AddTransient<ILicenseService, LicenseService>();
        services.AddTransient<ILicenseQueryService, LicenseQueryService>();
        services.AddTransient<License.ILicenseFactory, License.Factory>();

        services.AddDbContext<LicenseDbContext>(options =>
        {
            options.UseSqlite(configuration.GetConnectionString("LicensesConnectionString"),
                x => x.MigrationsAssembly(Assembly.GetExecutingAssembly().FullName));
        });
        
        services.AddScoped<ILicenseRepository, LicenseRepository>();
        services.AddScoped<ILicenseQueryRepository, LicenseQueryRepository>();
        services.AddScoped<ILicensorProvider, LicensorProvider>();
        
        return services;
    }
}
