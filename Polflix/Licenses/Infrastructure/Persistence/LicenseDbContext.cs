using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Storage;
using Polflix.Licenses.Infrastructure.Persistence.Licenses;
using Polflix.Licenses.Infrastructure.Persistence.Licensors;

namespace Polflix.Licenses.Infrastructure.Persistence;

public class LicenseDbContext : DbContext
{
    public DbSet<LicenseEntity> Licenses { get; set; } = default!;
    public DbSet<LicensorEntity> Licensors { get; set; } = default!;

    public LicenseDbContext(DbContextOptions<LicenseDbContext> options) : base(options)
    {
    }
    
    protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder) =>
        optionsBuilder.LogTo(Console.WriteLine);

    // Only for testing purposes
    public void Migrate()
    {
        Database.GetService<IRelationalDatabaseCreator>().CreateTables();
    }
    
    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        modelBuilder.Entity<LicenseEntity>()
            .ToTable("Licenses");

        modelBuilder.Entity<LicenseEntity>()
            .Property(x => x.Version).IsConcurrencyToken();
        
        modelBuilder.Entity<LicensorEntity>()
           .ToTable("Licensors");
        
        modelBuilder.Entity<OutboxMessageEntity>()
            .ToTable("Outbox")
            .HasKey(p => p.Position);
    }
}