namespace Polflix.Licenses.Infrastructure.Persistence.Licensors;

public class LicensorEntity
{
    public Guid Id { get; set; }
    public string Name { get; set; } = default!;
    public string Address { get; set; } = default!;
}