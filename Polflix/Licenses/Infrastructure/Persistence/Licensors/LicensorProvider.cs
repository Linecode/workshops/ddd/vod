using Microsoft.EntityFrameworkCore;
using Polflix.Licenses.Domain;
using Polflix.Licenses.Domain.Licensors;
using Polflix.Licenses.Infrastructure.Persistence.Mappers;

namespace Polflix.Licenses.Infrastructure.Persistence.Licensors;

public class LicensorProvider : ILicensorProvider
{
    private readonly LicenseDbContext _context;

    public LicensorProvider(LicenseDbContext context)
    {
        _context = context;
    }
    
    public async Task<Licensor> GetOrCreate(LicensorIdOrData licensorIdOrData, CancellationToken ct)
    {
        if (licensorIdOrData.LicensorId != null)
        {
            return (await _context.Licensors.AsNoTracking()
                       .SingleOrDefaultAsync(l => l.Id == licensorIdOrData.LicensorId, cancellationToken: ct))?.Map() ??
                   throw new InvalidOperationException();
        }

        var licensor = new LicensorEntity
        {
            Id = Guid.NewGuid(),
            Name = licensorIdOrData.Name,
            Address = licensorIdOrData.Address
        };
        
        _context.Licensors.Add(licensor);
        await _context.SaveChangesAsync(ct);

        return licensor.Map();
    }
}