using Polflix.Licenses.Domain;

namespace Polflix.Licenses.Infrastructure.Persistence.Licenses;

public interface ILicenseRepository
{
    Task GetAndUpdate(Guid id, Func<LicenseEntity?, LicenseEvent[]> handle, CancellationToken ct);
}