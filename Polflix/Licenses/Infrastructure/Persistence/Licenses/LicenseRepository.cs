using Polflix.Licenses.Domain;
using Polflix.Licenses.Infrastructure.Core.Events;

namespace Polflix.Licenses.Infrastructure.Persistence.Licenses;

public class LicenseRepository : EntityFrameworkRepository<LicenseEntity, LicenseEvent, LicenseDbContext>, ILicenseRepository
{
    public LicenseRepository(LicenseDbContext dbContext) : 
        base(dbContext, id => e => e.Id == id)
    {
    }

    protected override void Evolve(LicenseDbContext dbContext, LicenseEntity? current, EventEnvelope<LicenseEvent> eventEnvelope)
    {
        var @event = eventEnvelope.Event;
        var id = eventEnvelope.Metadata.RecordId;

        switch (@event)
        {
            case LicenseEvent.LicenseAcquired (var decryptionKey, var timeFrame, var territory, var licensorId):
            {
                dbContext.Licenses.Add(new LicenseEntity
                {
                    Id = id,
                    DecryptionKey = decryptionKey,
                    From = timeFrame.From,
                    To = timeFrame.To,
                    Territory = territory.Value,
                    LicensorId = licensorId
                });
                break;
            }
            
            case LicenseEvent.LicenseActivated:
                current.CurrentState = LicenseEntity.State.Active;
                break;
            
            case LicenseEvent.LicenseExpired:
                current.CurrentState = LicenseEntity.State.Expired;
                break;
            
            case LicenseEvent.LicenseRenewed (var decryptionKey, var timeFrame):
                current.CurrentState = LicenseEntity.State.Provisioned;
                current.From = timeFrame.From;
                current.To = timeFrame.To;
                current.DecryptionKey = decryptionKey;
                break;
        }
        
        if (current != null)
            current.Version++;
    }
}