using Microsoft.EntityFrameworkCore;

namespace Polflix.Licenses.Infrastructure.Persistence.Licenses;

public interface ILicenseQueryRepository
{
    Task<LicenseEntity?> FindById(Guid id, CancellationToken ct);
}

public class LicenseQueryRepository : ILicenseQueryRepository
{
    private readonly LicenseDbContext _context;

    public LicenseQueryRepository(LicenseDbContext context)
    {
        _context = context;
    }

    public Task<LicenseEntity?> FindById(Guid id, CancellationToken ct)
    {
        return _context.Licenses
            .AsNoTracking()
            .Include(l => l.Licensor)
            .Where(x => x.Id == id)
            .Select(x => x)
            .SingleOrDefaultAsync(ct);
    }
}