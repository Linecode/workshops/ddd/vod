using System.Text.Json.Serialization;
using Polflix.Licenses.Infrastructure.Persistence.Licensors;

namespace Polflix.Licenses.Infrastructure.Persistence.Licenses;

public class LicenseEntity
{
    public enum State { Provisioned, Active, Expired }
    
    public required Guid Id { get; set; }
    public State CurrentState { get; set; } = State.Provisioned;
    
    public string DecryptionKey { get; set; } = string.Empty;
    public DateTime From { get; set; }
    public DateTime To { get; set; }
    [JsonIgnore]
    public Guid LicensorId { get; set; }
    public LicensorEntity Licensor { get; set; } = default!;
    public int Territory { get; set; }
    
    public int Version { get; set; }
}