using System.Linq.Expressions;
using System.Text.Json;
using Microsoft.EntityFrameworkCore;
using Polflix.Licenses.Infrastructure.Core.Events;

namespace Polflix.Licenses.Infrastructure.Persistence.Licenses;

public abstract class EntityFrameworkRepository<TEntity, TEvent, TDbContext>
    where TEntity : class
    where TEvent : class
    where TDbContext : DbContext
{
    protected readonly TDbContext DbContext;
    private readonly Func<Guid, Expression<Func<TEntity, bool>>> _getId;

    protected EntityFrameworkRepository(TDbContext dbContext, Func<Guid, Expression<Func<TEntity, bool>>> getId)
    {
        DbContext = dbContext;
        _getId = getId;
    }
    
    public async Task GetAndUpdate(Guid id, Func<TEntity?, TEvent[]> handle, CancellationToken ct)
    {
        var entity = await Includes(DbContext.Set<TEntity>())
            .SingleOrDefaultAsync(_getId(id), ct);

        var events = handle(entity);

        ProcessEvents(
            DbContext,
            entity,
            events.Select(e => new EventEnvelope<TEvent>(e, new EventMetadata(id))).ToList()
        );

        await DbContext.SaveChangesAsync(ct);
    }

    private void ProcessEvents(TDbContext dbContext, TEntity? entity, IReadOnlyList<EventEnvelope<TEvent>> events)
    {
        var outbox = dbContext.Set<OutboxMessageEntity>();
        foreach (var eventEnvelope in events)
        {
            Evolve(dbContext, entity, eventEnvelope);
            outbox.Add(OutboxMessageEntity.From(Enrich(eventEnvelope, entity)));
        }
    }

    protected virtual IQueryable<TEntity> Includes(DbSet<TEntity> query) =>
        query;

    protected abstract void Evolve(TDbContext dbContext, TEntity? current, EventEnvelope<TEvent> eventEnvelope);

    protected virtual IEventEnvelope Enrich(EventEnvelope<TEvent> eventEnvelope, TEntity? _) => eventEnvelope;
}


public class OutboxMessageEntity
{
    public long Position { get; init; } = default;
    public required string MessageId { get; init; }
    public required string MessageType { get; init; }
    public required string Data { get; init; }
    public required DateTimeOffset Scheduled { get; init; }

    public static OutboxMessageEntity From(IEventEnvelope eventEnvelope) =>
        new()
        {
            MessageId = Guid.NewGuid().ToString(),
            Data = JsonSerializer.Serialize(eventEnvelope.Event, eventEnvelope.Event.GetType()),
            Scheduled = DateTimeOffset.UtcNow,
            MessageType = eventEnvelope.Event.GetType().FullName ?? "Unknown"
        };
}
