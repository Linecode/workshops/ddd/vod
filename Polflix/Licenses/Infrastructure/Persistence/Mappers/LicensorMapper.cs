using Polflix.Licenses.Domain;
using Polflix.Licenses.Infrastructure.Persistence.Licensors;

namespace Polflix.Licenses.Infrastructure.Persistence.Mappers;

public static class LicensorMapper
{
    public static Licensor Map(this LicensorEntity licensor)
        => new (licensor.Id, licensor.Name, licensor.Address);
}