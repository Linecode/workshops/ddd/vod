using Polflix.Licenses.Domain;
using Polflix.Licenses.Infrastructure.Persistence.Licenses;
using Polflix.Shared;

namespace Polflix.Licenses.Infrastructure.Persistence.Mappers;

public static class LicenseEntityMapper
{
    public static License MapToAggregate(this LicenseEntity license, License.ILicenseFactory licenseFactory)
        => licenseFactory.Create(
            (License.State)license.CurrentState,
            DateRange.New(license.From, license.To),
            license.DecryptionKey
            );
}