namespace Polflix.Licenses.Infrastructure.Core.Events;

public interface IEventEnvelope
{
    object Event { get; }
    EventMetadata Metadata { get; }
}

public record EventEnvelope<TEvent>(TEvent Event, EventMetadata Metadata): IEventEnvelope
    where TEvent: notnull
{
    object IEventEnvelope.Event => Event;
}

public record EventMetadata(Guid RecordId);