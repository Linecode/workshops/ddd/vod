using System.Reflection;
using System.Runtime.Serialization;
using System.Text.Json;
using FastEndpoints;
using FastEndpoints.Swagger;
using MassTransit;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Storage;
using Polflix.Catalog;
using Polflix.Catalog.Infrastructure.Persistence;
using Polflix.Library;
using Polflix.Licenses;
using Polflix.Licenses.Infrastructure.Persistence;
using Polflix.Licenses.Infrastructure.Persistence.Licenses;
using Polflix.SomeModule;
using Polflix.SomeModule.Infrastructure.Persistence;
using Polflix.Streaming;
using Polflix.Streaming.Infrastructure.Persistence;
using Serilog;

var builder = WebApplication.CreateBuilder(args);

builder.Host.UseSerilog((host, log) =>
{
    if (host.HostingEnvironment.IsProduction())
        log.MinimumLevel.Information();

    log.WriteTo.Console();
});

// This should be in module
var connectionString = builder.Configuration.GetConnectionString("DefaultConnection");
builder.Services.AddDbContext<SomeDbContext>(options => options.UseSqlite(connectionString));
    
builder.Services.AddMediatR(cfg =>
{
    cfg.RegisterServicesFromAssemblyContaining<SomeModule>();
});
builder.Services.AddFastEndpoints(o =>
{
    o.IncludeAbstractValidators = true;
});
builder.Services.SwaggerDocument(o =>
{
    o.DocumentSettings = s =>
    {
        s.Title = "Test Api";
        s.Version = "v1";
    };
});
builder.Services.RegisterSomeModuleServices()
    .RegisterLicenseModuleServices(builder.Configuration)
    .RegisterCatalogModuleServices(builder.Configuration)
    .RegisterStreamModuleServices(builder.Configuration)
    .RegisterLibraryModuleServices(builder.Configuration);

builder.Services.AddMassTransit(x =>
{
    x.SetKebabCaseEndpointNameFormatter();
    x.SetInMemorySagaRepositoryProvider();

    var entryAssembly = Assembly.GetEntryAssembly();
    x.AddConsumers(entryAssembly);
    x.AddSagaStateMachines(entryAssembly);
    x.AddSagas(entryAssembly);
    x.AddActivities(entryAssembly);
    
    x.UsingRabbitMq((context, cfg) =>
    {
        cfg.Host("localhost", "/", h =>
        {
            h.Username("user");
            h.Password("password");
        });

        cfg.ConfigureEndpoints(context);
    });
});

builder.Services.AddCors(_ =>
{
    _.AddPolicy("cors", policy =>
    {
        var allowedDomains = new List<string>();
        var exposedHeaders = new List<string>();

        builder.Configuration.GetSection("Cors:AllowedDomains")
            .Bind(allowedDomains);
        builder.Configuration.GetSection("Cors:ExposedHeaders")
            .Bind(exposedHeaders);

        policy.WithOrigins(allowedDomains.ToArray())
            .AllowAnyHeader()
            .WithExposedHeaders(exposedHeaders.ToArray())
            .AllowAnyMethod()
            .AllowCredentials();
    });
});

var app = builder.Build();

#region Database Setup

// This is not a production code, it just allows to recreate the database on startup
using var scope = app.Services.CreateScope();

var licenseContext = scope.ServiceProvider.GetRequiredService<LicenseDbContext>();

var catalogContext = scope.ServiceProvider.GetRequiredService<CatalogDbContext>();
var steamingContext = scope.ServiceProvider.GetRequiredService<StreamingDbContext>();

licenseContext.Database.EnsureDeleted();
licenseContext.Migrate();
catalogContext.Migrate();
steamingContext.Migrate();

#endregion

app.UseCors("cors");

app.UseFastEndpoints(c =>
{
    c.Serializer.Options.PropertyNamingPolicy = JsonNamingPolicy.CamelCase;
    c.Endpoints.RoutePrefix = "api";
});
app.UseSwaggerGen();

app.Run();
