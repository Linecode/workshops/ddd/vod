namespace Polflix.Library.Application;

public interface ILibraryFacade
{
    Task<bool> CanPlayContent(Guid contentId, Guid userId);
}

public class LibraryFacade : ILibraryFacade
{
    public Task<bool> CanPlayContent(Guid contentId, Guid userId)
    {
        return Task.FromResult(true); 
    }
}