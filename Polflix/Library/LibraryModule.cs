using Polflix.Library.Application;

namespace Polflix.Library;

public abstract class LibraryModule
{
    
}

public static class LibraryModuleExtensions
{
    public static IServiceCollection RegisterLibraryModuleServices(this IServiceCollection services,
        IConfiguration configuration)
    {
        services.AddTransient<ILibraryFacade, LibraryFacade>();
        
        return services;
    }
}