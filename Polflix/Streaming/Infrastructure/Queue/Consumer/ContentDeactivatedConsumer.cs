using MassTransit;
using Polflix.Contracts;
using Polflix.Streaming.Application;

namespace Polflix.Streaming.Infrastructure.Queue.Consumer;

public class ContentDeactivatedConsumer : IConsumer<ContentDeactivated>
{
    private readonly IStreamService _streamService;
    private readonly ILogger<ContentDeactivatedConsumer> _logger;

    public ContentDeactivatedConsumer(IStreamService streamService, ILogger<ContentDeactivatedConsumer> logger)
    {
        _streamService = streamService;
        _logger = logger;
    }
    
    public async Task Consume(ConsumeContext<ContentDeactivated> context)
    {
        var contentId = context.Message.ContentId;
        
        _logger.LogInformation("Received Event Content deactivated: {ContentId}", contentId);
        
        await _streamService.CompleteStream(contentId);
        // Based on this event we will need to notify browser that the content is no longer available
    }
}