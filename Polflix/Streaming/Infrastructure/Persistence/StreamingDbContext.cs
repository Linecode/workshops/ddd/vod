using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Storage;

namespace Polflix.Streaming.Infrastructure.Persistence;

public class StreamingDbContext : DbContext
{
    public DbSet<Model.Stream> Streams { get; set; } = default!;
    
    public StreamingDbContext(DbContextOptions<StreamingDbContext> options) : base(options)
    {
    }
    
    protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder) =>
        optionsBuilder.LogTo(Console.WriteLine);
    
    
    // Only for testing purposes
    public void Migrate()
    {
        Database.GetService<IRelationalDatabaseCreator>().CreateTables();
    }
}