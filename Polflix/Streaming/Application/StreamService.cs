using Polflix.Library.Application;
using Polflix.Streaming.Infrastructure.Persistence;

namespace Polflix.Streaming.Application;

public interface IStreamService
{
    Task<Model.Stream?> CreateStream(Guid contentId, Guid userId);
    Task CompleteStream(Guid streamId);
}

public class StreamService : IStreamService
{
    private readonly StreamingDbContext _streamingDbContext;
    private readonly ILibraryFacade _libraryFacade;

    public StreamService(StreamingDbContext streamingDbContext, ILibraryFacade libraryFacade)
    {
        _streamingDbContext = streamingDbContext;
        _libraryFacade = libraryFacade;
    }

    public async Task<Model.Stream?> CreateStream(Guid contentId, Guid userId)
    {
        if (await _libraryFacade.CanPlayContent(contentId, userId))
        {
            var stream = new Model.Stream
            {
                ContentId = contentId,
                UserId = userId,
                StartedAt = DateTime.UtcNow
            };
        
            _streamingDbContext.Streams.Add(stream);
            await _streamingDbContext.SaveChangesAsync();
        
            return stream;
        }
    
        return null;
    }

    public async Task CompleteStream(Guid streamId)
    {
        var stream = await _streamingDbContext.Streams.FindAsync(streamId);
        
        if (stream != null)
        {
            stream.EndedAt = DateTime.UtcNow;
            await _streamingDbContext.SaveChangesAsync();
        }
    }
}