using FastEndpoints;
using Polflix.Streaming.Application;

namespace Polflix.Streaming.Presentation;

public class StreamCreate : Endpoint<StreamCreateRequest>
{
    private readonly IStreamService _streamService;

    public StreamCreate(IStreamService streamService)
    {
        _streamService = streamService;
    }

    public override async Task HandleAsync(StreamCreateRequest req, CancellationToken ct)
    {
        var result = await _streamService.CreateStream(req.ContentId, Guid.NewGuid());

        await SendAsync(result, 200, ct);
    }

    public override void Configure()
    {
        Post("/stream");
        AllowAnonymous();
    }
}

public record StreamCreateRequest(Guid ContentId);