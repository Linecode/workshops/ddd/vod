using FastEndpoints;
using Polflix.Streaming.Application;

namespace Polflix.Streaming.Presentation;

public class StreamComplete : EndpointWithoutRequest
{
    private readonly IStreamService _streamService;

    public StreamComplete(IStreamService streamService)
    {
        _streamService = streamService;
    }

    public override async Task HandleAsync(CancellationToken ct)
    {
        var id = Route<Guid>("Id");
        
        await _streamService.CompleteStream(id);

        await SendAsync("", 200, ct);
    }

    public override void Configure()
    {
        Post("/stream/{Id}");
        AllowAnonymous();
    }
}