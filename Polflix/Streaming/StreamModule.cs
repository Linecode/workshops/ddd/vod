using System.Reflection;
using Microsoft.EntityFrameworkCore;
using Polflix.Streaming.Application;
using Polflix.Streaming.Infrastructure.Persistence;

namespace Polflix.Streaming;

public abstract class StreamModule
{
    
}

public static class StreamModuleExtensions
{
    public static IServiceCollection RegisterStreamModuleServices(this IServiceCollection services,
        IConfiguration configuration)
    {
        services.AddDbContext<StreamingDbContext>(options =>
        {
            options.UseSqlite(configuration.GetConnectionString("StreamConnectionString"));
        });
        services.AddScoped<IStreamService, StreamService>();
        
        return services;
    }
}