namespace Polflix.Streaming.Model;

public class Stream
{
    public Guid Id { get; set; } = Guid.NewGuid();
    
    public Guid ContentId { get; set; }
    public Guid UserId { get; set; }
    
    public DateTime StartedAt { get; set; }
    public DateTime? EndedAt { get; set; }

    public bool IsCompleted => EndedAt.HasValue;
}