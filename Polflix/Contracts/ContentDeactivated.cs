namespace Polflix.Contracts;

public interface ContentDeactivated
{
    Guid ContentId { get; }
}