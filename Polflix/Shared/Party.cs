namespace Polflix.Shared;

public class Party
{
    public Guid Id { get; private set; }
    public string Name { get; private set; }
    
    public Party(Guid id, string name)
    {
        Id = id;
        Name = name;
    }
}