using FastEndpoints;
using MediatR;
using Polflix.Catalog.Application.Contents;

namespace Polflix.Catalog.Presentation.Content;

public class ContentCreate : Endpoint<CreateContent>
{
    private readonly ISender _sender;

    public ContentCreate(ISender sender)
    {
        _sender = sender;
    }

    public override async Task HandleAsync(CreateContent req, CancellationToken ct)
    {
        var contentId = Guid.NewGuid();

        var (filePath, contentType) = req;

        await _sender.Send(new CreateContentCommand(contentId, filePath, contentType), ct);

        HttpContext.Response.Headers.Add("Location", $"/api/content/{contentId}");
        await SendAsync("", 200, ct);
    }

    public override void Configure()
    {
        Post("/content");
        AllowAnonymous();
    }
}

public record CreateContent(string FilePath, string ContentType);