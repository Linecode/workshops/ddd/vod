using FastEndpoints;
using MediatR;
using Polflix.Catalog.Application.Movies;

namespace Polflix.Catalog.Presentation.Movie;

public class MovieDeActivate : EndpointWithoutRequest
{
    private readonly ISender _sender;

    public MovieDeActivate(ISender sender)
    {
        _sender = sender;
    }
    
    public override async Task HandleAsync(CancellationToken cancellationToken)
    {
        var id = Route<Guid>("Id");
        
        await _sender.Send(new DeActivateMovieCommand(id), cancellationToken);

        await SendAsync("", 200, cancellationToken);
    }
    
    public override void Configure()
    {
        Post("/movie/{Id}/deactivate");
        AllowAnonymous();
    }
}