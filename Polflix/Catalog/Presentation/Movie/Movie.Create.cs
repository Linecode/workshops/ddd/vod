using FastEndpoints;
using MediatR;
using Polflix.Catalog.Application.Movies;
using Polflix.Catalog.Domain;
using Polflix.Catalog.Domain.Movies;

namespace Polflix.Catalog.Presentation.Movie;

public class MovieCreate : Endpoint<MovieCreateResource>
{
    private readonly ISender _sender;

    public MovieCreate(ISender sender)
    {
        _sender = sender;
    }

    public override async Task HandleAsync(MovieCreateResource req, CancellationToken ct)
    {
        var movieId = Guid.NewGuid();
        var (contentId, restriction, title, description, director, releaseDate) = req;

        var movieData = new MovieData
        {
            Description = description,
            Director = director,
            ReleaseDate = releaseDate,
            Title = title
        };
        var restrictionEntity = Restriction.From(restriction);

        await _sender.Send(new CreateMovieCommand(movieId, contentId, movieData, restrictionEntity), ct);
        
        await SendAsync("", 200, ct);
    }

    public override void Configure()
    {
        Post("/movie");
        AllowAnonymous();
    }
}

public record MovieCreateResource(Guid ContentId, string Restriction, string Title, string Description, string Director,
    DateOnly ReleaseDate);

