using FastEndpoints;
using MediatR;
using Polflix.Catalog.Application.Movies;

namespace Polflix.Catalog.Presentation.Movie;

public class MovieActivate : EndpointWithoutRequest
{
    private readonly ISender _sender;

    public MovieActivate(ISender sender)
    {
        _sender = sender;
    }

    public override async Task HandleAsync(CancellationToken ct)
    {
        var id = Route<Guid>("Id");
        
        await _sender.Send(new ActivateMovieCommand(id), ct);

        await SendAsync("", 200, ct);
    }

    public override void Configure()
    {
        Post("/movie/{Id}/activate");
        AllowAnonymous();
    }
}