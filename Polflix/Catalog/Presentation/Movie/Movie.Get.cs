using FastEndpoints;
using MediatR;
using Polflix.Catalog.Application.Movies;

namespace Polflix.Catalog.Presentation.Movie;

public class MovieGet : EndpointWithoutRequest
{
    private readonly ISender _sender;

    public MovieGet(ISender sender)
    {
        _sender = sender;
    }

    public override async Task HandleAsync(CancellationToken cancellationToken)
    {
        var id = Route<Guid>("Id");

        var result = await _sender.Send(new GetSingleMovieQuery(id), cancellationToken);

        await result.Match(
            async (x) => await SendAsync(x, 200, cancellationToken),
            async (_) => await SendAsync("", 404, cancellationToken)
        );
    }
    
    public override void Configure()
    {
        Get("/movie/{Id}");
        AllowAnonymous();
    }
}