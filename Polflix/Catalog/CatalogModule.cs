using Microsoft.EntityFrameworkCore;
using Polflix.Catalog.Infrastructure.Persistence;
using Polflix.Catalog.Infrastructure.Queue;

namespace Polflix.Catalog;

public abstract class CatalogModule
{
    
}

public static class CatalogModuleExtensions
{
    public static IServiceCollection RegisterCatalogModuleServices(this IServiceCollection services,
        IConfiguration configuration)
    {
        services.AddDbContext<CatalogDbContext>(options =>
            options.UseSqlite(configuration.GetConnectionString("CatalogConnectionString")));

        services.AddScoped<IContentRepository, ContentRepository>();
        services.AddScoped<IMoviesRepository, MoviesRepository>();

        services.AddScoped<IMessagePublisher, MessagePublisher>();

        return services;
    }
}