using EnsureThat;
using Polflix.Commons.Ddd;

namespace Polflix.Catalog.Domain;

public class Content : Entity<Guid>, AggregateRoot<Guid>
{
    public string FilePath { get; private set; }
    public string ContentType { get; private set; }
    
    private Content() { }

    public static Content Create(string filePath, string contentType)
    {
        Ensure.That(filePath).IsNotEmptyOrWhiteSpace();
        Ensure.That(contentType).IsNotEmptyOrWhiteSpace();
        
        var content = new Content
        {
            FilePath = filePath,
            ContentType = contentType
        };

        return content;
    }

    public static Content Create(Guid id, string filePath, string contentType)
    {
        var  content = Create(filePath, contentType);
        content.Id = id;

        return content;
    }
}