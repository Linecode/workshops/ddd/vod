using Polflix.Commons.Ddd;

namespace Polflix.Catalog.Domain;

public class Restriction : ValueObject<Restriction>
{
    public int MinimumAge { get; private set; }

    public Restriction(int minimumAge)
    {
        MinimumAge = minimumAge;
    }

    protected override IEnumerable<object> GetEqualityComponents()
    {
        yield return MinimumAge;
    }
    
    public override string ToString()
    {
        if (MinimumAge == 0) return string.Empty;

        return $"{MinimumAge}+";
    }

    public static Restriction Over18 => new (18);
    public static Restriction Over16 => new (16);
    public static Restriction Over13 => new (14);
    public static Restriction None => new (0);

    public static Restriction From(string value)
    {
        return value switch
        {
            "18+" => Over18,
            "16+" => Over16,
            "13+" => Over13,
            _ => None
        };
    }
}