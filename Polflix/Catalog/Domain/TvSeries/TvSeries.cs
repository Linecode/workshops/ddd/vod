using Polflix.Commons.Ddd;

namespace Polflix.Catalog.Domain.TvSeries;

public class TvSeries : Entity<Guid>, AggregateRoot<Guid>
{
    public enum State { Active, UnActive }
    
    public Restriction Restriction { get; private set; }
    
    private List<Season> _seasons = new();
    public IReadOnlyList<Season> Seasons => _seasons; 
    
    public TvSeriesData Data { get; private set; }

    public State CurrentState { get; private set; } = State.UnActive;

    private TvSeries() {}

    public IEnumerable<DomainEvent> Extend()
    {
        var lastSeason = _seasons.MaxBy(x => x.Number)?.Number ?? 0;
        var newSeason = lastSeason + 1;
        _seasons.Add(new Season(newSeason, new List<Content>()));

        return new[] { new Events.TvSeriesExtended(Id, newSeason) };
    }

    public IEnumerable<DomainEvent> ExtendSeason(int number, IEnumerable<Content> contents)
    {
        var season = _seasons.FirstOrDefault(x => x.Number == number);

        if (season is null) throw new InvalidOperationException("Season not exists");
        
        season.AddEpisodes(contents);
        
        return new[] { new Events.TvSeriesSeasonExtended(Id, number, contents.Select(x => x.Id).ToList()) };
    }

    public static TvSeries Create(TvSeriesData data, Restriction restriction)
    {
        var tvSeries = new TvSeries
        {
            Data = data,
            Restriction = restriction
        };

        return tvSeries;
    }

    public abstract record Events : DomainEvent
    {
        public record TvSeriesExtended(Guid TvSeriesId, int Season) : Events;
        
        public record TvSeriesSeasonExtended(Guid TvSeriesId, int Season, IList<Guid> Episodes) : Events;
    } 
}

public class TvSeriesData
{
    public string Title { get; set; }
    public string Description { get; set; }
    public string Director { get; set; }
}