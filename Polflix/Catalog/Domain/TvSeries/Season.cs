using Polflix.Commons.Ddd;

namespace Polflix.Catalog.Domain.TvSeries;

public class Season : Entity<Guid>
{
    public int Number { get; private set; }

    private List<Content> _episodes = new();
    public IReadOnlyCollection<Content> Episodes => _episodes;

    private Season() { }

    public Season(int number, List<Content> episodes)
    {
        Number = number;
        _episodes = episodes;
    }
    
    public void AddEpisodes(IEnumerable<Content> episodes)
        => _episodes.AddRange(episodes);
}