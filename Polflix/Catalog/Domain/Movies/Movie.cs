using Polflix.Commons.Ddd;

namespace Polflix.Catalog.Domain.Movies;

public class Movie : Entity<Guid>, AggregateRoot<Guid>
{
    public enum State { Active, UnActive }
    
    public Restriction Restriction { get; private set; }
    public Content Content { get; private set; }
    public State CurrentState { get; private set; } = State.UnActive;
    
    public MovieData Data { get; private set; }

    private Movie() { }

    public IEnumerable<DomainEvent> Activate()
    {
        CurrentState = State.Active;
        
        return new[] { new Events.MovieActivated(Id, Content.Id) };
    }
    
    public IEnumerable<DomainEvent> DeActivate()
    {
        CurrentState = State.UnActive;
        
        return new[] { new Events.MovieDeactivated(Id, Content.Id) };
    }

    public static Movie Create(string filePath, string contentType, Restriction restriction, MovieData data)
    {
        var movie = new Movie
        {
            Restriction = restriction,
            Content = Content.Create(filePath, contentType),
            Data = data
        };

        return movie;
    }

    public static Movie Create(Content content, Restriction restriction, MovieData data)
    {
        var movie = Create(content.FilePath, content.ContentType, restriction, data);
        
        return movie;
    }

    public static Movie Create(Guid id, Content content, Restriction restriction, MovieData data)
    {
        var movie = Create(content.FilePath, content.ContentType, restriction, data);
        movie.Id = id;
        
        return movie;
    }

    public abstract record Events : DomainEvent
    {
        public record MovieActivated(Guid MovieId, Guid ContentId) : Events;
        public record MovieDeactivated(Guid MovieId, Guid ContentId) : Events;
    }
}

// CRUD
public class MovieData
{
    public string Title { get; set; }
    public string Description { get; set; }
    public string Director { get; set; }
    public DateOnly ReleaseDate { get; set; }
}
