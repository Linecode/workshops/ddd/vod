using MediatR;
using Polflix.Catalog.Infrastructure.Persistence;

namespace Polflix.Catalog.Application.Movies;

public record ActivateMovieCommand(Guid MovieId) : IRequest
{
    private class ActivateMovieCommandHandler : IRequestHandler<ActivateMovieCommand>
    {
        private readonly IMoviesRepository _repository;

        public ActivateMovieCommandHandler(IMoviesRepository repository)
        {
            _repository = repository;
        }
        
        public async Task Handle(ActivateMovieCommand request, CancellationToken cancellationToken)
        {
            var movie = await _repository.FindById(request.MovieId);
            
            if (movie is null) throw new InvalidOperationException("Movie not found.");

            movie.Activate();
            
            _repository.Update(movie);
            await _repository.UnitOfWork.SaveEntitiesAsync(cancellationToken);
        }
    }
}