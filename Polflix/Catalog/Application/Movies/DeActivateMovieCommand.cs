using MediatR;
using Polflix.Catalog.Infrastructure.Persistence;
using Polflix.Catalog.Infrastructure.Queue;

namespace Polflix.Catalog.Application.Movies;

public record DeActivateMovieCommand(Guid MovieId) : IRequest
{
    private class DeActivateMovieCommandHandler : IRequestHandler<DeActivateMovieCommand>
    {
        private readonly IMoviesRepository _repository;
        private readonly IMessagePublisher _messagePublisher;

        public DeActivateMovieCommandHandler(IMoviesRepository repository, IMessagePublisher messagePublisher)
        {
            _repository = repository;
            _messagePublisher = messagePublisher;
        }
        
        public async Task Handle(DeActivateMovieCommand request, CancellationToken cancellationToken)
        {
            var movie = await _repository.FindById(request.MovieId);
            
            if (movie is null) throw new InvalidOperationException();

            var events = movie.DeActivate();
            
            _repository.Update(movie);
            await _repository.UnitOfWork.SaveEntitiesAsync(cancellationToken);

            // This is not transactional, should be done with outbox pattern
            foreach (var @event in events)
            {
                await _messagePublisher.Publish(@event, cancellationToken);
            }
        }
    }
}