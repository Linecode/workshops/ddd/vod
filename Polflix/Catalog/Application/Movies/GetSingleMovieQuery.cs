using MediatR;
using Polflix.Catalog.Domain.Movies;
using Polflix.Catalog.Infrastructure.Persistence;
using Polflix.Commons.Utils;

namespace Polflix.Catalog.Application.Movies;

public record GetSingleMovieQuery(Guid Id) : IRequest<Result<Movie>>
{
    private class GetSingleMovieQueryHandler : IRequestHandler<GetSingleMovieQuery, Result<Movie>>
    {
        private readonly IMoviesRepository _repository;

        public GetSingleMovieQueryHandler(IMoviesRepository repository)
        {
            _repository = repository;
        }
        
        public async Task<Result<Movie>> Handle(GetSingleMovieQuery request, CancellationToken cancellationToken)
        {
            var id = request.Id;

            var movie = await _repository.FindById(id);
            
            if (movie is null)
                return Result<Movie>.Error(new Exception("Movie not found"));

            return Result<Movie>.Success(movie);
        }
    }
}