using MediatR;
using Polflix.Catalog.Domain;
using Polflix.Catalog.Domain.Movies;
using Polflix.Catalog.Infrastructure.Persistence;

namespace Polflix.Catalog.Application.Movies;

public record CreateMovieCommand(Guid MovieId, Guid ContentId, MovieData Data, Restriction Restriction) : IRequest
{
    private class CreateMovieCommandHandler : IRequestHandler<CreateMovieCommand>
    {
        private readonly IMoviesRepository _repository;
        private readonly IContentRepository _contentRepository;

        public CreateMovieCommandHandler(IMoviesRepository repository, IContentRepository contentRepository)
        {
            _repository = repository;
            _contentRepository = contentRepository;
        }
        
        public async Task Handle(CreateMovieCommand request, CancellationToken cancellationToken)
        {
            var (movieId, contentId, data, restriction) = request;

            var content = await _contentRepository.Get(contentId);

            if (content == null) throw new InvalidOperationException();

            var movie = Movie.Create(movieId, content, restriction, data);
            
            _repository.Add(movie);
            await _repository.UnitOfWork.SaveEntitiesAsync(cancellationToken);
        }
    }
}