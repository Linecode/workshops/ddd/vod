using MediatR;
using Polflix.Catalog.Domain;
using Polflix.Catalog.Infrastructure.Persistence;

namespace Polflix.Catalog.Application.Contents;

public record CreateContentCommand(Guid ContentId, string FilePath, string ContentType) : IRequest
{
    private class CreateContentHandler : IRequestHandler<CreateContentCommand>
    {
        private readonly IContentRepository _repository;

        public CreateContentHandler(IContentRepository repository)
        {
            _repository = repository;
        }
        
        public async Task Handle(CreateContentCommand request, CancellationToken cancellationToken)
        {
            var (id, filePath, contentType) = request;
            var content = Content.Create(id, filePath, contentType);

            _repository.Add(content);
            await _repository.UnitOfWork.SaveEntitiesAsync(cancellationToken);
        }
    }
}