using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Storage;
using Polflix.Catalog.Domain;
using Polflix.Catalog.Domain.Movies;
using Polflix.Catalog.Infrastructure.Persistence.Configurations;
using Polflix.Commons.Ef;

namespace Polflix.Catalog.Infrastructure.Persistence;

public class CatalogDbContext : DbContext, IUnitOfWork
{
    public DbSet<Content> Contents { get; set; } = default!;
    public DbSet<Movie> Movies { get; set; } = default!;
    
    public CatalogDbContext(DbContextOptions<CatalogDbContext> options) : base(options)
    {
    }
    
    protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder) =>
        optionsBuilder.LogTo(Console.WriteLine);
    
    public async Task<bool> SaveEntitiesAsync(CancellationToken cancellationToken = default)
    {
        await SaveChangesAsync(cancellationToken);

        return true;
    }

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        modelBuilder.ApplyConfiguration(new ContentTypeConfiguration());
        modelBuilder.ApplyConfiguration(new MovieTypeConfiguration());
        
        base.OnModelCreating(modelBuilder);
    }
    
    // Only for testing purposes
    public void Migrate()
    {
        Database.GetService<IRelationalDatabaseCreator>().CreateTables();
    }
}