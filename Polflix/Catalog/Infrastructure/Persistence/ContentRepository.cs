using Microsoft.EntityFrameworkCore;
using Polflix.Catalog.Domain;
using Polflix.Commons.Ef;

namespace Polflix.Catalog.Infrastructure.Persistence;

public interface IContentRepository
{
    IUnitOfWork UnitOfWork { get; }
    void Add(Content content);
    Task<Content?> Get(Guid contentId);
}

public class ContentRepository : IContentRepository
{
    private readonly CatalogDbContext _context;

    public IUnitOfWork UnitOfWork => _context;

    public ContentRepository(CatalogDbContext context)
    {
        _context = context;
    }
    
    public void Add(Content content)
        => _context.Contents.Add(content);

    public Task<Content?> Get(Guid contentId)
    {
        return _context.Contents.FirstOrDefaultAsync(x => x.Id == contentId);
    }
}