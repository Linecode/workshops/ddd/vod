using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Polflix.Catalog.Domain;

namespace Polflix.Catalog.Infrastructure.Persistence.Configurations;

public class ContentTypeConfiguration : IEntityTypeConfiguration<Domain.Content>
{
    public void Configure(EntityTypeBuilder<Domain.Content> builder)
    {
        builder.HasKey(x => x.Id);

        builder.Property<byte[]>("Version").IsRowVersion();
        
        builder.ToTable("Contents");
    }
}