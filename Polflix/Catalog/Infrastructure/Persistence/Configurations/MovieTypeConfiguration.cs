using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Polflix.Catalog.Domain;
using Polflix.Catalog.Domain.Movies;

namespace Polflix.Catalog.Infrastructure.Persistence.Configurations;

public class MovieTypeConfiguration : IEntityTypeConfiguration<Movie>
{
    public void Configure(EntityTypeBuilder<Movie> builder)
    {
        builder.HasKey(m => m.Id);

        builder.Property<byte[]>("Version").IsRowVersion();

        builder.HasOne<Content>(x => x.Content);

        builder.OwnsOne(x => x.Data, d =>
        {
            d.Property(x => x.Title).HasColumnName("Title");
            d.Property(x => x.Description).HasColumnName("Description");
            d.Property(x => x.Director).HasColumnName("Director");
            d.Property(x => x.ReleaseDate).HasColumnName("ReleaseDate");
        });

        builder.Property(x => x.Restriction).HasConversion(r => r.ToString(), s => Restriction.From(s));

        builder.ToTable("Movies");
    }
}