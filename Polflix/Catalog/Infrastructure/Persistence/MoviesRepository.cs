using Microsoft.EntityFrameworkCore;
using Polflix.Catalog.Domain.Movies;
using Polflix.Commons.Ef;

namespace Polflix.Catalog.Infrastructure.Persistence;

public interface IMoviesRepository
{
    IUnitOfWork UnitOfWork { get; }
    void Add(Movie movie);
    Task<Movie?> FindById(Guid movieId);
    void Update(Movie movie);
}

public class MoviesRepository : IMoviesRepository
{
    private readonly CatalogDbContext _context;
    
    public IUnitOfWork UnitOfWork => _context;

    public MoviesRepository(CatalogDbContext context)
    {
        _context = context;
    }
    
    public void Add(Movie movie)
    {
        _context.Movies.Add(movie);
    }

    public Task<Movie?> FindById(Guid movieId)
        => _context.Movies
            .Include(x => x.Content)
            .FirstOrDefaultAsync(x => x.Id == movieId);
    
    public void Update(Movie movie)
    {
        _context.Movies.Update(movie);
    }
}