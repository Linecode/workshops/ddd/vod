using MassTransit;
using Polflix.Catalog.Domain.Movies;
using Polflix.Contracts;

namespace Polflix.Catalog.Infrastructure.Queue;

public interface IMessagePublisher
{
    ValueTask Publish<T>(T obj, CancellationToken ct = default);
}

public class MessagePublisher : IMessagePublisher
{
    private readonly IPublishEndpoint _publishEndpoint;
    private readonly ILogger<MessagePublisher> _logger;

    public MessagePublisher(IPublishEndpoint publishEndpoint, ILogger<MessagePublisher> logger)
    {
        _publishEndpoint = publishEndpoint;
        _logger = logger;
    }

    public async ValueTask Publish<T>(T obj, CancellationToken ct = default)
    {
        if (obj == null) return;
        
        _logger.LogInformation("Publishing message: {Message}", obj);
        
        // Layer where we could enhance / repack events
        switch (obj)
        {
            case Movie.Events.MovieDeactivated (var _, var contentId): 
                await _publishEndpoint.Publish<ContentDeactivated>(new { ContentId = contentId }, ct);
                break;
        }
    }
}